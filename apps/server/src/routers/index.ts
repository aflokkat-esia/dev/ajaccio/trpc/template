import { trpc } from "../trpc";
import { userRouter } from "./user";

export const trpcRouter = trpc.router({
  hello: trpc.procedure.query(() => {
    return "Hello from TRPC!";
  }),
  user: userRouter,
});
