"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("@trpc/server/adapters/express");
const cors_1 = __importDefault(require("cors"));
const express_2 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const prisma_1 = require("./prisma");
const index_1 = require("./routers/index");
const PORT = 3000;
const app = (0, express_2.default)();
const appRouter = express_2.default.Router();
app.use((0, cors_1.default)());
app.use(express_2.default.json());
app.use("/api", appRouter);
app.use("/trpc", (0, express_1.createExpressMiddleware)({ router: index_1.trpcRouter }));
app.use(express_2.default.static(path_1.default.join(__dirname, "../..", "client", "dist")));
appRouter.get("/", (_req, res) => {
    res.send("Hello from Rest API!");
});
appRouter.get("/users", (_req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const users = yield prisma_1.prisma.user.findMany();
    res.json(users);
}));
app.listen(PORT || 3000, () => {
    console.log(`Server listening on port ${PORT || 3000}`);
});
