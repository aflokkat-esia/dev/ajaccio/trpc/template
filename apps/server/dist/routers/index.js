"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.trpcRouter = void 0;
const trpc_1 = require("../trpc");
const user_1 = require("./user");
exports.trpcRouter = trpc_1.trpc.router({
    hello: trpc_1.trpc.procedure.query(() => {
        return "Hello from TRPC!";
    }),
    user: user_1.userRouter,
});
