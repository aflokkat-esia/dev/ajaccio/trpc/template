"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.userRouter = void 0;
const zod_1 = require("zod");
const prisma_1 = require("../prisma");
const trpc_1 = require("../trpc");
exports.userRouter = trpc_1.trpc.router({
    getAllUsers: trpc_1.trpc.procedure.query(() => __awaiter(void 0, void 0, void 0, function* () {
        const users = yield prisma_1.prisma.user.findMany();
        return users;
    })),
    getUserById: trpc_1.trpc.procedure
        .input(zod_1.z.object({ ID: zod_1.z.string().cuid() }))
        .query(({ input }) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield prisma_1.prisma.user.findFirst({
            where: {
                id: input.ID,
            },
        });
        return user;
    })),
    createUser: trpc_1.trpc.procedure
        .input(zod_1.z.object({ name: zod_1.z.string(), email: zod_1.z.string().email() }))
        .mutation(({ input }) => __awaiter(void 0, void 0, void 0, function* () {
        const user = yield prisma_1.prisma.user.create({
            data: {
                name: input.name,
                email: input.email,
            },
        });
        return user;
    })),
});
